# PDF Puppeteer

(name subject to change)

This is a tool that wraps the Google Chrome Puppeteer pdf functionality.

It limits what you can do with puppeteer but, is meant to add functionality for PDF files that are not present.

This suppresses puppeteers pdf at creating headers/footers and margins. Instead those are created and managed before purely in DOM.

This allows for features not present in puppeteer, including unique per-page headers/footers, eventually page numbering, table of contents, footnotes, and other useful features for pdf documents.

Primarily created for [ReLaXed](https://github.com/RelaxedJS/ReLaXed) to add features that are needed for a real document creation tool.

**NOTE** this is a really early build and is not meant for production. It is primarily a proof of concept. It is not on npmjs so installation requires cloning of this git project.

You can create a document, add pages, add body content to pages, add header/footer content, add right/left/top-right/top-left/bottom-right/bottom-left margin contents. Create new pages at specific indexes, save the pdf.

Basic usage: 

```javascript
const Document = require('./index.js')

var doc = new Document()
doc.on('ready', async () => {
  var page1 = await doc.newPage() // create the first page and get page object to manipulate with
  // Add some content
  await page1.addBody('<p>Some Test Body Text</p>')
  await page1.addHeader('Header')
  await page1.addFooter('Footer')

  // Create a new page and add some content
  var page2 = await page1.addPageAfter()
  await page2.addBody('<h1>Some Test Body Text on page 2</h1>')
  await page2.addHeader('Header 2')
  await page2.addFooter('Footer 2')

  await page2.addPageBefore() // Add a blank page between this page and the first page
  await doc.save('test.pdf') // Save the pdf
  await doc.close() // Close the document
})
```

## Planned Features

- Read an entire HTML document and format it appropriately.
- Add plugins and hooks for every process.
  - Plugins will allow for content generation, biliography, table of contents, index page, etc.