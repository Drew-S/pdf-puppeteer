const fs = require('fs')
const tmp = require('tmp')
const Minibus = require('minibus')
const Fixings = require('fixings')

const Page = require('./Page')
const Templates = require('./Templates')

const FORMATS = Templates.FORMATS
const TEMPLATE = Templates.TEMPLATE
const template = Templates.template
const defaultStyle = Templates.defaultStyle

const I_PX = 96
const CM_PX = 37.795276
const MM_PX = 3.779528

class Document {

  constructor(options, puppeteerPage=null) {
    options = options || {}
    this.margins = Object.assign({}, options.margins, {
      top: 14,
      bottom: 14,
      left: 14,
      right: 14
    })
    this.format = options.format || 'letter'
    if(options.size) this.size = options.size
    else this.size = FORMATS[this.format]

    this.Plate = new Fixings()

    if (options.plugins) this.Plate.addPlugins(options.plugins)

    this.currentPage = -1
    this.pageCount = 0

    this.$tmpFile = tmp.fileSync({ postfix: '.html' })
    fs.writeFileSync(this.$tmpFile.name, TEMPLATE.replace('{{STYLE}}', defaultStyle(this.margins, this.size)))

    this.$bus = Minibus.create()

    this.on('_complete_init', async () => {
      await this.$page.goto(`file://${this.$tmpFile.name}`, { waitUntil: 'networkidle0' })
      try {
        var Eater = await this.Plate.resolveHookAsync('document-ready')
        if (Eater) {
          var currentPlugin = await Eater.nextAsync()
          while (currentPlugin) {
            await currentPlugin.handler(this)
            currentPlugin = await Eater.nextAsync()
          }
        }
      } catch(error) {
        console.log(error)
      }
      this.emit('ready')
    })

    if (puppeteerPage === null) {
      (async () => { await this.init() })()
    } else {
      this.$page = puppeteerPage
      this.emit('_complete_init')
    }

  }

  async init() {
    this.$puppeteer = require('puppeteer')
    this.$browser = await this.$puppeteer.launch()
    this.$page = await this.$browser.newPage()
    this.$pageHTMLFormat = await this.$browser.newPage()
    this.$pageHTMLTransfer = await this.$browser.newPage()
    this.emit('_complete_init')
  }

  async fromHTMLString(html) {

    var Eater = null
    var currentPlugin = null

    try {
      Eater = await this.Plate.resolveHookAsync('document-fromHTML-before')
      if (Eater) {
        currentPlugin = await Eater.nextAsync()
        while (currentPlugin) {
          await currentPlugin.handler(html)
          currentPlugin = await Eater.nextAsync()
        }
      }
    } catch(error) {
      console.log(error)
    }

    var height = this.size.height - this.margins.top - this.margins.bottom

    if(!(
      /<html>/g.test(html)
      && /<\/html>/g.test(html)
      && /<body>/g.test(html)
      && /<\/body>/g.test(html))) return false

    this.$tmpFileHTMLFormat = tmp.fileSync({ postfix: '.html' })
    fs.writeFileSync(this.$tmpFileHTMLFormat.name, html)

    await this.$pageHTMLFormat.goto(`file://${this.$tmpFileHTMLFormat.name}`, { waitUntil: 'networkidle0' })

    var next = async () => {
      return await this.$pageHTMLFormat.$eval('body', body => {
        if (!body.firstElementChild) return false
        var fc = body.firstElementChild
        body.removeChild(fc)
        return fc.outerHTML
      })
    }

    var page = await this.getCurrentPage()
    if (page === null) page = await this.newPage()

    var out = await next()
    while (out) {
      var elCap = /<(.*)>[\w\s]*<\//.exec(out)
      var el = null
      if (elCap) el = elCap[1]
      try {
        Eater = await this.Plate.resolveHookAsync(`fromHTML-add-element:${el}`)
        if (Eater) {
          currentPlugin = await Eater.nextAsync()
          while (currentPlugin) {
            await currentPlugin.handler(out)
            currentPlugin = await Eater.nextAsync()
          }
        }
      } catch(error) {
        console.log(error)
      }
      var results = await page.addBody(out)
      var compHeight = height + this.margins.top + (height + this.margins.top + this.margins.bottom)*this.currentPage
      if (results >= compHeight*MM_PX) {
        try {
          Eater = await this.Plate.resolveHookAsync(`fromHTML-overflow-element:${el}`)
          if (Eater) {
            currentPlugin = await Eater.nextAsync()
          }
        } catch(error) {
          console.log(error)
        }
        if (currentPlugin) {
          await page.removeBody()
          var newPage = await this.newPage()
          while (currentPlugin) {
            await currentPlugin.handler(out, page, newPage)
            currentPlugin = await Eater.nextAsync()
          }
          page = newPage
        } else {
          await page.removeBody()
          page = await this.newPage()
          await page.addBody(out)
        }
      }
      out = await next()
    }

    try {
      Eater = await this.Plate.resolveHookAsync('document-fromHTML-before')
      if (Eater) {
        currentPlugin = await Eater.nextAsync()
        while (currentPlugin) {
          await currentPlugin.handler(html)
          currentPlugin = await Eater.nextAsync()
        }
      }
    } catch(error) {
      console.log(error)
    }
  }

  async fromHTMLFile(file) {
    return new Promise((resolve, reject) => {
      fs.readFile(file, (error, data) => {
        if (error) reject(error)
        else resolve(this.fromHTMLString(data.toString()))
      })
    })
  }

  async newPage() {
    await this.$page.$eval('body', (body, htmlString) => {
      var div = document.createElement('div')
      div.innerHTML = htmlString.trim()
      body.appendChild(div.firstElementChild)
    }, template(this.pageCount))
    this.currentPage = this.pageCount
    this.pageCount++
    return await this.getCurrentPage()
  }

  async newPageAt(index) {
    if (index < 0) return false
    if (index === this.pageCount) return this.newPage()
    await this.$page.$eval('body', (body, htmlString, index) => {
      var div = document.createElement('div')
      div.innerHTML = htmlString.trim()
      body.insertBefore(div.firstElementChild, body.children[index])
      var ind = index
      for (var i of body.children) {
        if (i.id === `pdf-puppeteer-page-${ind}`) {
          ind++
          i.id = `pdf-puppeteer-page-${ind}`
        }
      }
      var el = body.querySelector(`#pdf-puppeteer-page-${index}-tmp`)
      el.id = `#pdf-puppeteer-page-${index}`
    }, template(index + '-tmp'), index)
    this.pageCount++
    this.currentPage = index
    return await this.getCurrentPage()
  }

  async getPage(index) {
    if (index < 0 || index >= this.pageCount) {
      throw new Error('Out of range')
    } else
      return new Page(this.$page, index, this)
  }

  async getCurrentPage() {
    if(this.currentPage === -1) return null
    return await this.getPage(this.currentPage)
  }

  async close() {
    await this.$browser.close()
  }

  async addStyleSheetString(css) {
    await this.$page.$eval('head', (head, cssString) => {
      var style = document.createElement('style')
      style.innerText = cssString
      head.appendChild(style)
    }, css)
  }

  async addStyleSheet(path) {
    var cssString = fs.readFileSync(path).toString()
    await this.addStyleSheetString(cssString)
  }

  async save(path) {
    // console.log(await this.$page.$eval('html', el => el.outerHTML))
    await this.$page.pdf({
      path: path,
      format: this.format,
      printBackground: true,
      width: `${this.size.width}mm`,
      height: `${this.size.height}mm`,
      margin: { top: '0px', left: '0px', right: '0px', bottom: '0px' }
    })
  }

  on(eventName, callback) {
    this.$bus.on(eventName, callback)
  }

  emit(eventName, ...args) {
    this.$bus.emit(eventName, args)
  }

  off(eventName) {
    this.$bus.off(eventName)
  }

}

exports.Document = Document