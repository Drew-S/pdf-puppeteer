const Minibus = require('minibus')

const PRE = 'pdf-puppeteer-'

class Page {
  constructor(page, index, document) {
    this.$page = page
    this.$indexString = `#${PRE}page-${index}`
    this.$index = index
    this.$document = document

    this.$bus = Minibus.create()
  }

  async __addTo(pos, ind, content) {
    return await this.$page.$eval(this.$indexString, (el, HTML, index, pos) => {
      var container = el.querySelector(pos)
      var div = document.createElement('DIV')
      div.innerHTML = HTML.trim()
      var newEl = div.firstChild

      if (index <= -2) return false

      if (index === -1) container.appendChild(newEl)
      else if (index >= container.children.length) return false
      else container.insertBefore(newEl, container.children[index])

      return newEl.offsetHeight + newEl.getBoundingClientRect().top
    }, content, ind, pos)
  }

  async __getFrom(pos) { await this.$page.$eval(this.$indexString, (el, pos) => {
    var container = el.querySelector(pos)
    return container.innerHTML
  }, pos) }

  async __removeFrom(pos, index) {
    return await this.$page.$eval(this.$indexString, (el, pos, index) => {
      var container = el.querySelector(pos)
      var child
      if (index <= -2) return false

      if (index === -1) {
        child = container.lastChild
        container.removeChild(child)
      } else if (index >= container.children.length) return false
      else {
        child = container.children[index]
        container.removeChild(child)
      }
      return child.outerHTML
    }, pos, index)
  }

  async addBody(string, index=-1) { return await this.__addTo(`.${PRE}body`, index, string) }
  async getBody() { return await this.__getFrom(`.${PRE}body`) }
  async removeBody(index=-1) { return await this.__removeFrom(`.${PRE}body`, index) }

  async addHeader(string, index=-1) { return await this.__addTo(`.${PRE}header`, index, string) }
  async getHeader() { return await this.__getFrom(`.${PRE}header`) }
  async removeHeader(index=-1) { return await this.__removeFrom(`.${PRE}header`, index) }

  async addFooter(string, index=-1) { return await this.__addTo(`.${PRE}footer`, index, string) }
  async getFooter() { return await this.__getFrom(`.${PRE}footer`) }
  async removeFooter(index=-1) { return await this.__removeFrom(`.${PRE}footer`, index) }

  async addRightMargin(string, index=-1) { return await this.__addTo(`.${PRE}right-margin`, index, string) }
  async getRightMargin() { return await this.__getFrom(`.${PRE}right-margin`) }
  async removeRightMargin(index=-1) { return await this.__removeFrom(`.${PRE}right-margin`, index) }

  async addLeftMargin(string, index=-1) { return await this.__addTo(`.${PRE}left-margin`, index, string) }
  async getLeftMargin() { return await this.__getFrom(`.${PRE}left-margin`) }
  async removeLeftMargin(index=-1) { return await this.__removeFrom(`.${PRE}left-margin`, index) }

  async addTopLeftMargin(string, index=-1) { return await this.__addTo(`.${PRE}top-left-margin`, index, string) }
  async getTopLeftMargin() { return await this.__getFrom(`.${PRE}top-left-margin`) }
  async removeTopLeftMargin(index=-1) { return await this.__removeFrom(`.${PRE}top-left-margin`, index) }

  async addBottomLeftMargin(string, index=-1) { return await this.__addTo(`.${PRE}bottom-left-margin`, index, string) }
  async getBottomLeftMargin() { return await this.__getFrom(`.${PRE}bottom-left-margin`) }
  async removeBottomLeftMargin(index=-1) { return await this.__removeFrom(`.${PRE}bottom-left-margin`, index) }

  async addTopRightMargin(string, index=-1) { return await this.__addTo(`.${PRE}top-right-margin`, index, string) }
  async getTopRightMargin() { return await this.__getFrom(`.${PRE}top-right-margin`) }
  async removeTopRightMargin(index=-1) { return await this.__removeFrom(`.${PRE}top-right-margin`, index) }

  async addBottomRightMargin(string, index=-1) { return await this.__addTo(`.${PRE}bottom-right-margin`, index, string) }
  async getBottomRightMargin() { return await this.__getFrom(`.${PRE}bottom-right-margin`) }
  async removeBottomRightMargin(index=-1) { return await this.__removeFrom(`.${PRE}bottom-right-margin`, index) }

  async nextPage() {
    return await this.$document.getPage(this.$index + 1)
  }
  
  async prevPage() {
    return await this.$document.getPage(this.$index - 1)
  }

  async addPageAfter() {
    return await this.$document.newPageAt(this.$index + 1)
  }
  
  async addPageBefore() {
    return await this.$document.newPageAt(this.$index)
  }

  on(event, callback) {
    this.$bus.on(event, callback)
  }

  emit(event, ...args) {
    this.$bus.emit(event, args)
  }

  // off(event) {
  //   this.$bus.off(event)
  // }
}

module.exports = Page