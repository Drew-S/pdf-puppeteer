const PRE = 'pdf-puppeteer-'

exports.FORMATS = {
  'letter': {
    height: 279.4,
    width: 215.9
  }
}

exports.TEMPLATE = `
<html>
  <head>
    <style>
      {{STYLE}}
    </style>
  </head>
  <body></body>
</html>`

exports.template = function(pg) {
  return `
  <div class='${PRE}page' id='${PRE}page-${pg}'>
    <div class='${PRE}top-margin'>
      <div class='${PRE}top-left-margin red'></div>
      <div class='${PRE}header blue'></div>
      <div class='${PRE}top-right-margin red'></div>
    </div>
    <div class='${PRE}main'>
      <div class='${PRE}left-margin blue'></div>
      <div class='${PRE}body red'></div>
      <div class='${PRE}right-margin blue'></div>
    </div>
    <div class='${PRE}bottom-margin'>
      <div class='${PRE}bottom-left-margin red'></div>
      <div class='${PRE}footer blue'></div>
      <div class='${PRE}bottom-right-margin red'></div>
    </div>
  </div>`
}

exports.defaultStyle = function(margins, size) {
  return `
  body, div {
    border: 0px;
    margin: 0px;
    padding: 0px;
    width: ${size.width}mm;
  }
  .blue {
    background-color: blue;
  }
  .red {
    background-color: red;
  }
  .${PRE}page {
    width: ${size.width}mm;
    height: ${size.height}mm;
    float: left;
  }
  .${PRE}top-margin {
    height: ${margins.top}mm;
    width: ${size.width}mm;
    float: left;
  }
  .${PRE}top-left-margin {
    height: ${margins.top}mm;
    width: ${margins.left}mm;
    float: left;
    overflow: hidden;
  }
  .${PRE}header {
    height: ${margins.top}mm;
    width: ${size.width - margins.left - margins.right}mm;
    float: left;
    overflow: hidden;
  }
  .${PRE}top-right-margin {
    height: ${margins.top}mm;
    width: ${margins.right}mm;
    float: left;
    overflow: hidden;
  }
  .${PRE}main {
    height: ${size.height - margins.top - margins.bottom}mm;
    width: ${size.width}mm;
    float: left;
  }
  .${PRE}left-margin {
    height: ${size.height - margins.top - margins.bottom}mm;
    width: ${margins.left}mm;
    float: left;
    overflow: hidden;
  }
  .${PRE}body {
    height: ${size.height - margins.top - margins.bottom}mm;
    width: ${size.width - margins.left - margins.right}mm;
    float: left;
    overflow: hidden;
  }
  .${PRE}right-margin {
    height: ${size.height - margins.top - margins.bottom}mm;
    width: ${margins.right}mm;
    float: left;
    overflow: hidden;
  }
  .${PRE}bottom-margin {
    height: ${margins.bottom}mm;
    width: ${size.width}mm;
    float: left;
  }
  .${PRE}bottom-left-margin {
    height: ${margins.bottom}mm;
    width: ${margins.left}mm;
    float: left;
    overflow: hidden;
  }
  .${PRE}footer {
    height ${margins.bottom}mm;
    width: ${size.width - margins.left - margins.right}mm;
    float: left;
    overflow: hidden;
  }
  .${PRE}bottom-right-margin {
    height: ${margins.bottom}mm;
    width: ${margins.right}mm;
    float: left;
    overflow: hidden;
  }
  `
}